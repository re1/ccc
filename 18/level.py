from util import read_all, process_data_array

if __name__ == '__main__':
    path = 'level-4/level4_2'
    data = read_all(path + '.in')

    with open(path + '.out', 'w') as file:
        file.write(data)
