""" I can ride my bike with no handlebars """

import math
import operator


class Cube(object):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


def read_all(path: str):
    with open(path) as file:
        s = ''              # string to return
        file.readline()     # skip first
        lines = file.read().split('\n')[:-1]
        grid = [list(map(float, line.split(' '))) for line in lines]

        boxes = []

        for y, row in enumerate(grid):
            for x, col in enumerate(row):
                if col > 0:
                    x2 = x
                    y2 = y

                    while(grid[y][x2] == col):   # find end in columns
                        x2 += 1

                    while(grid[y2][x] == col):   # find end in rows
                        y2 += 1

                    dy = y2 - y
                    dx = x2 - x
                    for i in range(dy + 1):     # clear processed visited columns
                        for j in range(dx + 1):
                            grid[y + i][x + j] = 0

                    if dx > 3 and dy > 3:
                        box = process_box(x, y, x2, y2)
                        boxes += [box]
                        print(y, x, '→', y2, x2, '→', col, '→', box)

        # s += str(n) + ' ' + str(res[0]) + ' ' + str(res[1]) + ' '
        # for box in boxes:

        # boxes.sort()
        not_so_sorted_boxes = list(sorted(boxes, key=lambda box: box[0]))
        sorted_boxes_reverse = list(
            sorted(boxes, key=operator.itemgetter(1, 0)))
        sorted_boxes = list(sorted(boxes, key=operator.itemgetter(0, 1)))
        print(boxes)
        print(not_so_sorted_boxes)
        print(sorted_boxes_reverse)
        print(sorted_boxes)

        for i, box in enumerate(sorted_boxes):
            s += "%i %i %i " % (i, box[0], box[1])

        print(s)
        return s


def process_data_array(data: list):
    data_list = list(set(data))
    data_list.sort()
    data_list.remove(0)
    return data_list


def process_data_line_lvl2(a_x, a_y, b_x, b_y, r):
    a_x += 0.5
    a_y += 0.5
    b_x += 0.5
    b_y += 0.5

    vec_x = b_x - a_x
    vec_y = b_y - a_y

    # Add ration
    vec_rx = vec_x * r
    vec_ry = vec_y * r

    # Add ze ursprung
    vec_rx_u = vec_rx + a_x
    vec_ry_u = vec_ry + a_y

    return (int(vec_rx_u), int(vec_ry_u))


def process_data_line_lvl3(a_x, a_y, b_x, b_y):
    a_x -= 0.5
    a_y -= 0.5
    b_x -= 0.5
    b_y -= 0.5

    delta_x = math.fabs(b_x-a_x)
    delta_y = math.fabs(b_y-a_y)

    data_list = []

    # ze shitty sonderfall
    if delta_x == 0:
        if b_y - a_y >= 0:
            for i in range(int(a_y + 0.5), int(b_y + 0.5) + 1):
                data_list += [int(a_x + 0.5), i]
                # data_list = data_list[::-1]
        else:
            for i in range(int(b_y + 0.5), int(a_y + 0.5) + 1):
                data_list.insert(0, int(a_x + 0.5))
                data_list.insert(1, i)
                # data_list += [int(a_x + 0.5), i]
                # data_list = data_list[::-1]
                # data_list.reverse()

        return data_list

    m_val = delta_y / delta_x

    signX = 1
    signY = 1

    if a_x > b_x:
        signX = -1

    if a_y > b_y:
        signY = -1

    print("delta_x: " + str(delta_x) + "; delta_y: " +
          str(delta_y) + "; m_val: " + str(m_val))

    if m_val <= 1:
        for i in range(0, int(delta_x) + 1):
            data_list += [math.ceil(a_x + i * signX),
                          math.ceil(a_y + i * m_val * signY)]
    else:
        for i in range(0, int(delta_y) + 1):
            data_list += [math.ceil(a_x + i / m_val * signX),
                          math.ceil(a_y + i * signY)]

    return data_list


def collect_boxes(grid):
    # found_heights = set()
    found_elems = list()

    for y, row in enumerate(grid):
        for x, col in enumerate(row):
            if col > 0:
                # check if we already found it
                # if col in found_heights:
                    # continue
                for found_elem in found_elems:
                    # print("bra: " + str(found_elem))
                    # print("bra1: " + str(found_elem[0]))
                    # print("bra2: " + str(found_elem[1]))

                    if x >= found_elem[0][0] and y >= found_elem[0][1] and x <= found_elem[1][0] and y <= found_elem[1][1]:
                        continue

                # print("bra")
                # do detect phase
                start_coors = [x, y]
                end_coors = [0, 0]
                # found_next_elem = False
                next_x = x
                next_y = y

                while True:
                    # check x+1, y+1
                    next_col_val = grid[next_y + 1][next_x + 1]
                    if next_col_val == col:
                        next_x += 1
                        next_y += 1
                        continue
                    # else try only y+1
                    next_col_val = grid[next_y + 1][next_x]
                    if next_col_val == col:
                        next_y += 1
                        continue
                    # else try only x+1
                    next_col_val = grid[next_y][next_x + 1]
                    if next_col_val == col:
                        next_x += 1
                        continue

                    # otherwise we have found it
                    end_coors[0] = next_x
                    end_coors[1] = next_y
                    break

                # found_heights.add(col)
                found_elems.append([start_coors, end_coors])
    return found_elems


def process_box(x1, y1, x2, y2):
    vec_x = (x2 - x1) / 2
    vec_y = (y2 - y1) / 2

    # add ze ursprung
    vec_ux = vec_x + x1
    vec_uy = vec_y + y1

    return (int(vec_uy), int(vec_ux))
