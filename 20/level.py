import numpy as np
import math

from sympy import Point, Polygon
from scipy import ndimage
from scipy import spatial

from util import FlightFile, lla_to_ecef_1, read_all, read_csv, test_func


def level1():
    path = 'level1/level1_5'
    # reads data as list of lists of 4 float values (time, lat, long, alt)
    data = read_all(path + '.in')

    matrix = np.matrix(data)

    mat_mean = math.floor(matrix.mean())
    mat_max = matrix.max()
    mat_min = matrix.min()

    print(mat_min)
    print(mat_max)
    print(mat_mean)

    timestamp_list = []
    lat_list = []
    long_list = []
    altitude_list = []

    # Fill data here
    for entry in data:
        timestamp_list.append(int(entry[0]))
        lat_list.append(entry[1])
        long_list.append(entry[2])
        altitude_list.append(entry[3])

    minTimestamp = np.min(timestamp_list)
    maxTimestamp = np.max(timestamp_list)
    minLat = np.min(lat_list)
    maxLat = np.max(lat_list)
    minLong = np.min(long_list)
    maxLong = np.max(long_list)
    maxAlititude = np.max(altitude_list)

    with open(path + '.out', 'w') as file:
        file.write('%i %i\n%g %g\n%g %g\n%g' % (
            minTimestamp, maxTimestamp, minLat, maxLat, minLong, maxLong, maxAlititude))


def level2():
    path = 'level2/level2_5'
    # reads data as list of lists of 4 float values (time, lat, long, alt)
    data = read_all(path + '.in')

    timestamp_list = []
    lat_list = []
    long_list = []
    altitude_list = []
    start_list = []
    end_list = []
    takeoff = []

    # Fill data here
    for entry in data:
        timestamp_list.append(int(entry[0]))
        lat_list.append(float(entry[1]))
        long_list.append(float(entry[2]))
        altitude_list.append(float(entry[3]))
        start_list.append(entry[4])
        end_list.append(entry[5])
        takeoff.append(int(entry[6]))

    full_flight_pair = []
    for i in range(0, len(start_list)):
        full_flight_pair.append((start_list[i], end_list[i], takeoff[i]))

    unique_full_flights = np.unique(full_flight_pair, axis=0)

    flight_pair = []

    for i in range(0, len(unique_full_flights)):
        flight_pair.append(
            (unique_full_flights[i][0], unique_full_flights[i][1]))

    unique, counts = np.unique(flight_pair, return_counts=True, axis=0)

    out_lines = []
    for i in range(0, len(unique)):
        start = unique[i][0]
        end = unique[i][1]
        count = counts[i]
        out_lines.append('%s %s %i\n' % (start, end, count))

    out_lines.sort()

    with open(path + '.out', 'w') as file:
        for i in range(0, len(out_lines)):
            file.write(out_lines[i])


def level3():
    path = 'level3/level3_5'
    # reads data as list of lists of 4 float values (lat, long, alt)
    data = read_all(path + '.in')

    lat_list = []
    long_list = []
    altitude_list = []

    # Fill data here
    for entry in data:
        lat_list.append(float(entry[0]))
        long_list.append(float(entry[1]))
        altitude_list.append(float(entry[2]))

    out_list = []

    for i in range(0, len(altitude_list)):
        (x, y, z) = lla_to_ecef_1(
            lat_list[i] * math.pi/180, long_list[i] * math.pi/180, altitude_list[i])
        out_list.append((x, y, z))

    with open(path + '.out', 'w') as file:
        for i in range(0, len(out_list)):
            file.write('%.10f %.10f %.10f\n' %
                       (out_list[i][0], out_list[i][1], out_list[i][2]))


def level4():
    path = 'level4/level4_example'
    # reads data as list of lists of 4 float values (lat, long, alt)
    flights = read_all(path + '.in')
    # flights is a list of lists of flightId and timestamp
    # print(flights)

    # latdict = {}
    # timestamp_list = []
    # lat_list = []
    # long_list = []
    # alt_list = []
    out_list = []

    for (flight_id, timestamp) in flights:
        flight_file = read_csv('level4/usedFlights/%i.csv' % flight_id)
        print(flight_id, timestamp)
        # Markus (Nix gut diese)
        # for (pstamp, plat, plong, palt) in flight_file.checkpoints_orig:
        #     timestamp_list.append(pstamp)
        #     lat_list.append(plat)
        #     long_list.append(plong)
        #     alt_list.append(palt)
        #     latdict[pstamp] = plat

        # print('Interp Lat', np.interp(timestamp, timestamp_list, lat_list))
        # print('Interp Long', np.interp(timestamp, timestamp_list, long_list))
        # print('Interp Alt', np.interp(timestamp, timestamp_list, alt_list))

        # Kevin :)
        (lat, long, alt) = flight_file.calc_interpolation_at_timestamp_geo(timestamp)
        out_list.append((lat, long, alt))
        print((lat, long, alt))

    with open(path + '.out', 'w') as file:
        for (lat, long, alt) in out_list:
            file.write('%.10f %.10f %.10f\n' %
                       (lat, long, alt))


if __name__ == '__main__':
    level4()
    test_func()
