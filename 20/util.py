import numpy as np
import math
import pyproj
from typing import Dict, Tuple, Sequence, List


def lla_to_ecef_1(lat, lon, alt):
    # see http://www.mathworks.de/help/toolbox/aeroblks/llatoecefposition.html
    rad = np.float64(6371000.0)        # Radius of the Earth (in meters)
    f = np.float64(1.0/298.257223563)  # Flattening factor WGS84 Model
    f = 0  # haxaxax
    cosLat = np.cos(lat)
    sinLat = np.sin(lat)
    FF = (1.0-f)**2
    C = 1/np.sqrt(cosLat**2 + FF * sinLat**2)
    S = C * FF

    x = (rad * C + alt)*cosLat * np.cos(lon)
    y = (rad * C + alt)*cosLat * np.sin(lon)
    z = (rad * S + alt)*sinLat
    return x, y, z


def lla_to_ecef_2(lat, lon, alt):
    ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    x, y, z = pyproj.transform(lla, ecef, lon, lat, alt, radians=False)

    return x, y, z


def ecef_to_lla(x, y, z):
    ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    lon, lat, alt = pyproj.transform(ecef, lla, x, y, z, radians=False)
    return lon, lat, alt


def deg_lla_to_ecef_1(lat, lon, alt):
    return lla_to_ecef_1(lat * math.pi/180, lon * math.pi/180, alt)


def deg_lla_to_ecef_2(lat, lon, alt):
    return lla_to_ecef_2(lat * math.pi/180, lon * math.pi/180, alt)


def xyz2llh(x, y, z):
    '''
    Function to convert xyz ECEF to llh
    convert cartesian coordinate into geographic coordinate
    ellipsoid definition: WGS84
      a= 6,378,137m
      f= 1/298.257

    Input
      x: coordinate X meters
      y: coordinate y meters
      z: coordinate z meters
    Output
      lat: latitude rad
      lon: longitude rad
      h: height meters
    '''
    # --- WGS84 constants
    a = 6371000.0
    f = 1.0 / 298.257223563
    f = 0  # haxaxax
    # --- derived constants
    b = a - f*a
    e = math.sqrt(math.pow(a, 2.0)-math.pow(b, 2.0))/a
    clambda = math.atan2(y, x)
    p = math.sqrt(pow(x, 2.0)+pow(y, 2))
    h_old = 0.0
    # first guess with h=0 meters
    theta = math.atan2(z, p*(1.0-math.pow(e, 2.0)))
    cs = math.cos(theta)
    sn = math.sin(theta)
    N = math.pow(a, 2.0)/math.sqrt(math.pow(a*cs, 2.0)+math.pow(b*sn, 2.0))
    h = p/cs - N
    while abs(h-h_old) > 1.0e-6:
        h_old = h
        theta = math.atan2(z, p*(1.0-math.pow(e, 2.0)*N/(N+h)))
        cs = math.cos(theta)
        sn = math.sin(theta)
        N = math.pow(a, 2.0)/math.sqrt(math.pow(a*cs, 2.0)+math.pow(b*sn, 2.0))
        h = p/cs - N
    # llh = {'lon': clambda, 'lat': theta, 'height': h}
    return clambda, theta, h


def test_func():
    test_deg_lon = 60.3412
    test_deg_lat = 65.1451
    test_alt = 1035

    (x, y, z) = lla_to_ecef_2(test_deg_lat, test_deg_lon, test_alt)

    (res_rad_lon, res_rad_lat, res_alt) = ecef_to_lla(x, y, z)
    res_deg_lon = res_rad_lon
    res_deg_lat = res_rad_lat
    print("lon", res_deg_lon)
    print("lat", res_deg_lat)
    print("alt", res_alt)


def read_all(path: str):
    with open(path) as file:
        entries = file.readline()  # read entries

        lines = file.read().split('\n')[:-1]

        grid = []

        for line in lines:
            grid.append(list(map(int, line.split(' '))))

        print(grid)

        return grid


def write_all(path: str, s: str):
    with open(path + '.out', 'w') as file:
        file.write(s)


class FlightFile:
    @staticmethod
    def _helper_convert(origin_entry: Tuple[float, float, float, float]):
        print("Values", origin_entry)
        (timestamp, fl_lat, fl_long, fl_alt) = origin_entry
        # (x, y, z) = deg_lla_to_ecef_1(fl_lat, fl_long, fl_alt)
        (x, y, z) = lla_to_ecef_2(fl_lat, fl_long, fl_alt)
        return (int(timestamp), x, y, z)

    def __init__(self, origin_airport: str, dest_airport: str, take_off_timestamp: int, coords: List[Tuple[float, float, float, float]]):
        self.origin_airport = origin_airport
        self.dest_airport = dest_airport
        self.take_off_timestamp = take_off_timestamp
        self.checkpoints_orig = coords
        self.checkpoints = []
        for i in range(0, len(coords)):
            self.checkpoints.append(FlightFile._helper_convert(coords[i]))

        super().__init__()

    def calc_interpolation_at_timestamp(self, timestamp: int):
        rel_timestamp = timestamp - self.take_off_timestamp
        print("Timestamp", timestamp)
        print("Rel Timestamp", rel_timestamp)
        print("Lowest", self.checkpoints[0])
        print("Highest", self.checkpoints[len(self.checkpoints) - 1])

        for i in range(0, len(self.checkpoints)):
            next_coord = self.checkpoints[i]
            next_coord_timestamp = next_coord[0]
            if next_coord_timestamp > rel_timestamp:
                prev_coord = self.checkpoints[i - 1]
                prev_coord_timestamp = prev_coord[0]

                rel_progress = (rel_timestamp - float(prev_coord_timestamp)) / (
                    float(next_coord_timestamp) - float(prev_coord_timestamp))

                vec_x = (next_coord[1] - prev_coord[1]) * rel_progress
                vec_y = (next_coord[2] - prev_coord[2]) * rel_progress
                vec_z = (next_coord[3] - prev_coord[3]) * rel_progress

                print("Rel progress", rel_progress)
                print("Vec x", vec_x)
                print("Vec y", vec_y)
                print("Vec z", vec_z)
                print("Prev Timestamp", prev_coord[0])
                print("Prev x", prev_coord[1])
                print("Prev y", prev_coord[2])
                print("Prev z", prev_coord[3])
                print("Next Timestamp", next_coord[0])
                print("Next x", next_coord[1])
                print("Next y", next_coord[2])
                print("Next z", next_coord[3])

                return (prev_coord[1] + vec_x, prev_coord[2] + vec_y, prev_coord[3] + vec_z)

    def calc_interpolation_at_timestamp_geo(self, timestamp: int):
        (x, y, z) = self.calc_interpolation_at_timestamp(timestamp)
        print("Target x", x)
        print("Target y", y)
        print("Target z", z)

        # (fl_long, fl_lat, fl_alt) = xyz2llh(x, y, z)
        (fl_long, fl_lat, fl_alt) = ecef_to_lla(x, y, z)

        print("Rad long:", fl_long)
        print("Rad lat:", fl_lat)
        print("Alt:", fl_alt)

        return fl_lat, fl_long, fl_alt


def read_csv(path: str):
    with open(path) as file:
        originAirport = file.readline()
        destinationAirport = file.readline()
        takeOffTimestamp = file.readline()
        countlines = file.readline()

        lines = file.read().split('\n')[:-1]

        grid = []

        for line in lines:
            grid.append(tuple(list(map(float, line.split(',')))))

        flightFile = FlightFile(
            originAirport, destinationAirport, int(takeOffTimestamp), grid)

        return flightFile
