class Bank:
    def __init__(self):
        self.transactions = {}

    def __str__(self):
        out = str(len(self.transactions.values()))
        for t_id, value in self.transactions.items():
            out += t_id + " " + str(len(value["in"].items()))

            for t_in_id, t_in in value["in"].items():
                out += " " + t_in_id + " " + t_in[0] + " " + t_in[1]

            for t_out_to, t_out_amount in value["out"].items():
                out += " " + t_out_to + " " + t_out_amount

            out += " " + value["time"] + "\n"

        return out


def read_all(path: str):
    bank = Bank()

    with open(path) as file:
        t_c = int(file.readline())

        for t_c in range(t_c):
            transaction_line = file.readline().replace("\n", "")
            transaction_model = transaction_line.split(" ")

            t_id = transaction_model[0]
            bank.transactions[t_id] = {
                "in": {},
                "out": {},
                "time": transaction_model[-1]
            }

            t_in_c = int(transaction_model[1])
            t_in_end_i = 2 + 3 * t_in_c

            t_in = transaction_model[2:t_in_end_i]
            t_in_id = t_in[0]
            t_in_owner = t_in[1]
            t_in_amount = t_in[2]
            print(t_in)
            bank.transactions[t_id]["in"][t_in_id] = [t_in_owner, t_in_amount]

            t_out_c = int(transaction_model[t_in_end_i])
            t_out_end_i = t_in_end_i + 1 + 2 * t_out_c

            t_out = transaction_model[t_in_end_i + 1:t_out_end_i]
            t_out_to = t_out[0]
            t_out_amount = t_out[1]
            print(t_out)
            bank.transactions[t_id]["out"][t_out_to] = t_out_amount

        file.close()

    return bank
