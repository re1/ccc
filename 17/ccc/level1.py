from ccc.util import read_all

if __name__ == "__main__":
    bank = read_all("level1/level1-1.txt")
    bank.execute_transactions()
    print(str(bank))

    with open("level1/results/level1-1.txt", "w+") as file:
        file.write(str(bank) + "\n")
        file.close()
