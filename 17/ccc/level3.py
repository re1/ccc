from ccc.util2 import read_all

if __name__ == "__main__":
    bank = read_all("level3/level3-1.txt")
    print(str(bank))

    with open("level3/results/level3-1.txt", "w+") as file:
        file.write(str(bank) + "\n")
        file.close()
