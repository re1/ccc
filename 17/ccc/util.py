""" I can ride my bike with no handlebars """
import re


class Account:
    def __init__(self, aid, owner, balance, limit):
        self.id = aid
        self.owner = owner
        self.balance = balance
        self.limit = limit
        self.transactions = {}

    def __str__(self):
        return self.owner + " " + str(self.balance)


class Bank:
    def __init__(self):
        self.accounts = {}

    def transact(self, sender, recipient, value):
        if value < self.accounts[sender].limit:
            print(sender, recipient, value)
            self.accounts[sender].balance -= value
            self.accounts[recipient].balance += value
        else:
            print("Limit reached on " + str(sender))

    def execute_transactions(self):
        queries = {}

        for account in self.accounts.values():
            print("Hi: " + str(account))
            for recipient, (value, time) in account.transactions.items():
                print("Hi2")
                queries[time] = lambda: self.transact(account.id, recipient, value)

        print(queries)
        for key in sorted(queries.keys()):
            queries[key]()

    def __str__(self):
        out = str(len(self.accounts))
        for account in self.accounts.values():
            out += "\n" + str(account)

        return out


def read_all(path: str):
    bank = Bank()

    with open(path) as file:
        account_count = int(file.readline())
        for i in range(account_count):
            transaction_line = file.readline().replace("\n", "")
            (owner, aid, balance, limit) = transaction_line.split(" ")

            if re.search('CAT\d\d\w{10}', aid):
                print(aid)
                valid = True

                chars = {}

                acc_id = aid[5:] + "CAT00"
                check = 0

                for c in acc_id:
                    check += ord(c)

                check %= 97
                check = 98 - check

                if not check == int(aid[3:5]):
                    valid = False

                for char in aid[5:]:
                    if chars.get(char):
                        chars[char] += 1
                    else:
                        chars[char] = 1

                print(chars)

                for char in chars.keys():
                    if char.isupper():
                        if chars[char] != chars.get(char.lower()):
                            valid = False
                    else:
                        if chars[char] != chars.get(char.upper()):
                            valid = False

                if valid:
                    bank.accounts[aid] = Account(aid, owner, int(balance), int(limit))

        transaction_count = int(file.readline())
        for i in range(transaction_count):
            transaction_line = file.readline().replace("\n", "")
            print(transaction_line)
            (sender, recipient, amount, time) = transaction_line.split(" ")

            if bank.accounts.get(sender) and bank.accounts.get(recipient):
                bank.accounts[sender].transactions[recipient] = [int(amount), int(time)]
            else:
                print(sender + " or " + recipient + " not found")

        file.close()

    return bank
