from ccc.util import read_all

if __name__ == "__main__":
    bank = read_all("level2/level2-eg.txt")
    bank.execute_transactions()
    print("========")
    print(str(bank))

    with open("level2/results/level2-eg.txt", "w+") as file:
        file.write(str(bank) + "\n")
        file.close()
