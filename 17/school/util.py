class Network:
    def __init__(self):
        self.nodes = {}
        self.pools = {}
        self.conns = {}

    def add_node(self, node_id: str, node_hr: int):
        self.nodes[node_id] = node_hr

    def add_pool(self, pool_id: str, nodes: list):
        self.pools[pool_id] = nodes

    def add_conn(self, node_id1, node_id2, latency):
        self.conns[node_id1 + "-" + node_id2] = latency

    def sum_nodes(self):
        return sum(self.nodes.values())

    def sum_pool(self, pool_id):
        n = 0
        for node in self.pools[pool_id]:
            n += self.nodes[node]
        return n

    def sum_pools(self):
        out = ""
        for pool_id in self.pools.keys():
            out += pool_id
            n = 0
            for node in self.pools[pool_id]:
                n += self.nodes[node]

            out += " " + str(n) + " "

        return out

    def node_hr_percent(self, node_id):
        return self.nodes[node_id] / self.sum_nodes() * 100

    def pool_hr_percent(self, pool_id):
        return self.sum_pool(pool_id) / self.sum_nodes() * 100

    def nodes_hr_percents(self):
        out = ""
        for node_id in self.nodes.keys():
            out += node_id
            value = str(round(self.node_hr_percent(node_id), 2))
            out += " " + value

            if len(value.split(".")[1]) < 2:
                out += "0"
            out += " "

        return out

    def pools_hr_percents(self):
        out = ""
        for pool_id in self.pools.keys():
            out += pool_id
            value = str(round(self.pool_hr_percent(pool_id), 2))
            out += " " + value
            if len(value.split(".")[1]) < 2:
                out += "0"
            out += " "

        return out

    def reach50(self, node_id):
        time = 0
        hr = self.nodes[node_id]
        hr50 = self.sum_nodes() / 2

        conns = {}

        for conn_id in self.conns.keys():
            if node_id in conn_id:
                (n1, n2) = conn_id.split("-")
                if n1 == node_id:
                    node_id = n2
                else:
                    node_id = n1

                conns[node_id] = self.conns[conn_id]

        for node_id in conns.keys():
            time += conns[node_id]
            hr += self.nodes[node_id]

            if hr > hr50:
                return time

            time += self.reach50(node_id)

    def reach50_all(self):
        out = ""
        for node_id in self.nodes.keys():
            out += node_id + " " + str(self.reach50(node_id)) + " "

        return out


def read_all(path: str):
    network = Network()

    with open(path) as file:
        node_count = int(file.readline())

        for i in range(node_count):
            node_line = file.readline().replace("\n", "")
            (node_id, node_hr) = node_line.split(" ")

            network.add_node(node_id, int(node_hr))

        pool_count = int(file.readline())

        for i in range(pool_count):
            pool_line = file.readline().replace("\n", "")
            pool = pool_line.split(" ")

            network.add_pool(pool[0], pool[1:])

        conn_count = int(file.readline())

        for i in range(conn_count):
            conn_line = file.readline().replace("\n", "")
            (node_id1, node_id2, latency) = conn_line.split(" ")

            network.add_conn(node_id1, node_id2, int(latency))

        file.close()
    return network
