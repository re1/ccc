# My submissions to the Catalysts Coding Contest

https://www.codingcontest.org/en/

- `20`: 03-04-2020 with @KevinW1998 and Damian :+1:

- `19`: 08-11-2019 with @KevinW1998 :+1:

- `18`: 16-11-2018 with @KevinW1998 :+1:

- `17`: 20-10-2017

- `16`: 31-03-2017
