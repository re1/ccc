stops = {}
journeys = []

faster = 0

def distance(from_location, to_location):
    x = float(from_location[1]) - float(to_location[1])
    y = float(from_location[2]) - float(to_location[2])

    return (x ** 2 + y ** 2) ** (1 / 2)

with open("level3-4.txt") as file:
    # Locations
    location_number = file.readline().replace("\n", "")
    for i in range(int(location_number)):
        line = file.readline().replace("\n", "")
        stop = line.split(" ")
        stops[stop[0]] = stop
        print("Added location: " + str(stop))

    # Journeys
    journey_number = file.readline().replace("\n", "")

    for i in range(int(journey_number)):
        line = file.readline().replace("\n", "")
        journey = line.split(" ")
        journeys.append(journey)
        print("Added journey: " + str(journey))

    # Hyper
    line = file.readline().replace("\n", "").split(" ")
    from_hyper = line[0]
    to_hyper = line[1]
    print("Using %s - %s" % (from_hyper, to_hyper))
    # Hyper time
    hyper_time = distance(stops[from_hyper], stops[to_hyper]) / 250.0 + 200.0
    print("Hyper: " + str(round(hyper_time)))

    for journey in journeys:
        from_journey = journey[0]
        to_journey = journey[1]
        time = journey[2]

        if distance(stops[from_journey], stops[from_hyper]) >= distance(stops[from_journey], stops[to_hyper]):
            print(distance(stops[from_journey], stops[from_hyper]))
            print(distance(stops[from_journey], stops[to_hyper]))

            foo = from_hyper

            from_hyper = to_hyper
            to_hyper = foo

        # Journey time
        journey_time1 = distance(stops[from_journey], stops[from_hyper]) / 15.0
        print("Journey1:", journey_time1)
        journey_time2 = distance(stops[to_hyper], stops[to_journey]) / 15.0
        print("Journey2:", journey_time2)
        journey_time = journey_time1 + journey_time2
        print("Journey: " + str(round(journey_time)))
        # Full
        full_time = journey_time + hyper_time
        print("Full: " + str(round(full_time)))
        print("")

        if full_time < float(journey[2]):
            print("Faster!")
            faster += 1

print(faster)
