import sys
import threading

stops = {}
journeys = []
best = []

location_number = None
journey_number = None
target_number = None


def distance(from_location, to_location):
    x = float(from_location[1]) - float(to_location[1])
    y = float(from_location[2]) - float(to_location[2])

    return (x ** 2 + y ** 2) ** (1 / 2)


def check_journey(from_hyper, to_hyper):
    # Count faster
    faster = 0
    # Hyper time
    hyper_time = distance(stops[from_hyper], stops[to_hyper]) / 250.0 + 200.0
    print("Hyper: " + str(round(hyper_time)))
    # Count faster
    faster = 0

    for journey in journeys:
        journey_time = float(journey[2])

        if hyper_time > journey_time:
            continue

        from_journey = journey[0]
        to_journey = journey[1]

        if distance(stops[from_journey], stops[from_hyper]) >= distance(stops[from_journey], stops[to_hyper]):
            foo = from_hyper
            from_hyper = to_hyper
            to_hyper = foo

        # Drive time
        drive1 = distance(stops[from_journey], stops[from_hyper]) / 15.0
        drive2 = distance(stops[to_hyper], stops[to_journey]) / 15.0
        drive_time = drive1 + drive2
        # Full
        full_time = drive_time + hyper_time

        if full_time < journey_time:
            faster += 1

    print(faster, target_number)
    if faster >= target_number:
        if [from_hyper, to_hyper] not in best:
            print("Best hyperloop: %s %s" % (from_hyper, to_hyper))
            best.append([from_hyper, to_hyper])
            sys.exit()

with open("level4-2.txt") as file:
    # Locations
    location_number = file.readline().replace("\n", "")
    for i in range(int(location_number)):
        line = file.readline().replace("\n", "")
        stop = line.split(" ")
        stops[stop[0]] = stop
        print("Added location: " + str(stop))

    # Journeys
    journey_number = file.readline().replace("\n", "")

    for i in range(int(journey_number)):
        line = file.readline().replace("\n", "")
        journey = line.split(" ")
        journeys.append(journey)
        print("Added journey: " + str(journey))

    # Hyper
    target_number = int(file.readline().replace("\n", ""))
    print("\nTarget number\n", target_number)

for from_hyper in stops:
    for to_hyper in stops:
        if from_hyper == to_hyper:
            continue

        threading.Thread(target=check_journey, args=(from_hyper, to_hyper)).start()

print(best)