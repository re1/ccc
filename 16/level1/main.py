import math


with open("level1-4.txt") as file:
    stops = {}
    location_number = file.readline().replace("\n", "")

    for i in range(int(location_number)):
        line = file.readline().replace("\n", "")
        stop = line.split(" ")
        stops[stop[0]] = stop
        print("Added " + str(stop))

    line = file.readline().replace("\n", "")
    from_to = line.split(" ")

    from_location = from_to[0]
    to_location = from_to[1]
    print("Go from %s to %s" % (from_location, to_location))

    travel_time = 0

    x = float(stops[from_location][1]) - float(stops[to_location][1])
    y = float(stops[from_location][2]) - float(stops[to_location][2])

    travel_time = (float(x**2) + float(y**2))**(1/2) / 250.0 + 200.0

    print(round(travel_time))

"""
def hyperloopTime(Bratislava, Brno):
 distance(Bratislava Brno) / 250.0 + 200.0
126633.9 / 250.0 + 200.0
706.5
"""
