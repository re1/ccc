import math


stops = {}

def distance(from_location, to_location):
    x = float(from_location[1]) - float(to_location[1])
    y = float(from_location[2]) - float(to_location[2])

    return (x ** 2 + y ** 2) ** (1 / 2)

with open("level2-4.txt") as file:
    location_number = file.readline().replace("\n", "")

    for i in range(int(location_number)):
        line = file.readline().replace("\n", "")
        stop = line.split(" ")
        stops[stop[0]] = stop
        print("Added " + str(stop))

    # Journey
    line = file.readline().replace("\n", "").split(" ")

    from_journey = line[0]
    to_journey = line[1]
    print("Go from %s to %s" % (from_journey, to_journey))

    # Hyper
    line = file.readline().replace("\n", "").split(" ")

    from_hyper = line[0]
    to_hyper = line[1]
    print("Using %s - %s" % (from_hyper, to_hyper))

    if distance(stops[from_journey], stops[from_hyper]) >= distance(stops[from_journey], stops[to_hyper]):
        print(distance(stops[from_journey], stops[from_hyper]))
        print(distance(stops[from_journey], stops[to_hyper]))

        foo = from_hyper

        from_hyper = to_hyper
        to_hyper = foo

        print(from_hyper, to_hyper)

    # Hyper time
    hyper_time = distance(stops[from_hyper], stops[to_hyper]) / 250.0 + 200.0
    print("Hyper: " + str(round(hyper_time)))
    # Journey time
    journey_time1 = distance(stops[from_journey], stops[from_hyper]) / 15.0
    print("Journey1:", journey_time1)
    journey_time2 = distance(stops[to_hyper], stops[to_journey]) / 15.0
    print("Journey2:", journey_time2)
    journey_time = journey_time1 + journey_time2
    print("Journey: " + str(round(journey_time)))

    # Full
    print("Full: " + str(round(journey_time + hyper_time)))
