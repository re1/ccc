from util import read_all
import numpy as np
import math
from sympy import Point, Polygon
from scipy import ndimage
from scipy import spatial

def level1():
    path = 'level-1/level1_5'
    data = read_all(path + '.in')

    matrix = np.matrix(data)

    mat_mean = math.floor(matrix.mean())
    mat_max = matrix.max()
    mat_min = matrix.min()

    print(mat_min)
    print(mat_max)
    print(mat_mean)

    with open(path + '.out', 'w') as file:
        file.write('%d %d %d' % (mat_min, mat_max, mat_mean))

def level2():
    path = 'level-2/level2_5'
    data = read_all(path + '.in')

    mapped_adj = {}
    def add_adj_entry(from_adj, to_adj):
        if from_adj == to_adj:
            return
        opt_elem = mapped_adj.get(from_adj, set())
        opt_elem.add(to_adj)
        mapped_adj[from_adj] = opt_elem


    matrix = np.matrix(data)
    rows = np.size(matrix, 0)
    cols = np.size(matrix, 1)

    countries = [0] * (matrix.max() + 1)

    # unique_elems = np.unique(matrix)
    # print(unique_elems)

    for y in range(rows):
        for x in range(cols):
            country_id = matrix.item((y, x))
            
            new_country_id = 0
            is_adj = False

            # Check top
            if y - 1 >= 0:
                new_country_id = matrix.item((y - 1, x))
                if country_id != new_country_id and not is_adj:
                    is_adj = True
            else:
                if not is_adj:
                    is_adj = True
                
            # Check left
            if x - 1 >= 0:
                new_country_id = matrix.item((y, x - 1))
                if country_id != new_country_id and not is_adj:
                    is_adj = True
            else:
                if not is_adj:
                    is_adj = True

            # Check right
            if x + 1 < cols:
                new_country_id = matrix.item((y, x + 1))
                if country_id != new_country_id and not is_adj:
                    is_adj = True
            else:
                if not is_adj:
                    is_adj = True

            # Check down
            if y + 1 < rows:
                new_country_id = matrix.item((y + 1, x))
                if country_id != new_country_id and not is_adj:
                    is_adj = True
            else:
                if not is_adj:
                    is_adj = True

            # print('(%d, %d) id %d -> %r' % (x, y, country_id, is_adj))

            # add value to country list
            if is_adj:
                countries[country_id] += 1

    # print(countries)

    with open(path + '.out', 'w') as file:
        file.write('\n'.join(map(lambda x: str(x), countries)))

def level3():
    path = 'level-3/level3_5'
    data = read_all(path + '.in')

    matrix = np.matrix(data)
    rows = np.size(matrix, 0)
    cols = np.size(matrix, 1)

    countries = [[]] * (matrix.max() + 1)
    capitals = []
    
    for y in range(rows):
        for x in range(cols):
            country_id = matrix.item((y, x))
            
            new_country_id = 0
            is_adj = False

            # Check top
            if y - 1 >= 0:
                new_country_id = matrix.item((y - 1, x))
                if country_id != new_country_id and not is_adj:
                    is_adj = True
            else:
                if not is_adj:
                    is_adj = True
                
            # Check left
            if x - 1 >= 0:
                new_country_id = matrix.item((y, x - 1))
                if country_id != new_country_id and not is_adj:
                    is_adj = True
            else:
                if not is_adj:
                    is_adj = True

            # Check right
            if x + 1 < cols:
                new_country_id = matrix.item((y, x + 1))
                if country_id != new_country_id and not is_adj:
                    is_adj = True
                
            else:
                if not is_adj:
                    is_adj = True

            # Check down
            if y + 1 < rows:
                new_country_id = matrix.item((y + 1, x))
                if country_id != new_country_id and not is_adj:
                    is_adj = True
            else:
                if not is_adj:
                    is_adj = True

            # print('(%d, %d) id %d -> %r' % (x, y, country_id, is_adj))

            # add value to country list
            if is_adj:
                countries[country_id].append((y, x))
    
    print('Matrix')
    # print(matrix)
    
    for i, country in enumerate(countries):
        print('Country %d' % i)

        cp_matrix = np.matrix(matrix, copy=True)
        cp_matrix[cp_matrix != i] = -1
        cp_matrix[cp_matrix == i] = -2

        cp_matrix[cp_matrix == -1] = 0
        cp_matrix[cp_matrix == -2] = 1

        # print(cp_matrix)
        
        mass = ndimage.measurements.center_of_mass(cp_matrix.A)

        (cx, cy) = (int(mass[1]), int(mass[0])) # ROUND?
        # print('%d %d' % (cx, cy))

        for tpl_border in country:
            cp_matrix.itemset(tpl_border, 0)

        # print(cp_matrix) 
        if(cp_matrix.item((cy, cx)) == 1):
            print("Country %d coor: (%d, %d)" % (i, cx, cy))
            capitals += [[cx, cy]]
        else:
            space_list = []
            for y in range(rows):
                for x in range(cols):
                    if cp_matrix.item((y, x)) == 1:
                        space_list += [(x, y)]
            
            # print(space_list)

            tree = spatial.KDTree(space_list)
            # print("Result for query %d %d" % (cx, cy))
            output = tree.query([cx, cy], 5)
            output_distances = output[0]
            output_idxs = output[1]

            if(output_distances[0] == output_distances[4]):
                raise ValueError("SAME >:(")
            if(output_distances[0] == output_distances[1]):
                print('Conflicts')
                print(output_distances[0])
                print(output_distances[1])
                print(output_distances[2])
                print(output_distances[3])

                print(space_list[output_idxs[0]])
                print(space_list[output_idxs[1]])
                print(space_list[output_idxs[2]])
                print(space_list[output_idxs[3]])
                # Gather all conflicts
                conflicts = []
                for i in range(len(output_idxs)):
                    if(output_distances[0] == output_distances[i]):
                        conflicts.append(space_list[output_idxs[i]])
                
                sorted(conflicts, key=lambda element: (element[1], element[0]))
                print(conflicts)

                (sorted_tx, sorted_ty) = conflicts[0]

                print('Conflict resolved through %d %d' % (sorted_tx, sorted_ty))

                capitals += [[sorted_tx, sorted_ty]]
            else:
                (target_x, target_y) = space_list[output_idxs[0]]
                print("Else %d %d" % (target_x, target_y))
                capitals += [[target_x, target_y]]

            # (distance, list_idx) = tree.query([cx, cy])

        # points = list(map(Point, country))
        # print(points)

    print('Capitals')
    # print(capitals)
    
    s = ''

    for capital in capitals:
        s += '%d %d\n' % (capital[0], capital[1])

    print(s)

    with open(path + '.out', 'w') as file:
        file.write(s)

if __name__ == '__main__':
    level3()
