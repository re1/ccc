# read file at path as grid array of corrdinate arrays
def read_all(path: str):
    with open(path) as file:
        # read number of rows and cols
        (rows, cols) = file.readline().split(' ')
        # read lines as array by splitting newline characters
        lines = file.read().split('\n')[:-1]
        # init grid array
        grid = []
        # add coordinates as list to grid array
        for line in lines:
            grid += [list(map(lambda x: int(x), line.split(' ')))[1::2]]
        # print grid for debugging purposes
        print(grid)

        return grid
